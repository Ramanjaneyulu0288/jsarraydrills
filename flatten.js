let nestedArray = [1, [2], [[3]], [[[4]]],[5]]; 



function flat(input,depth = 1,newArr){
    for (let item of input){
        if(Array.isArray(item) && depth > 0){
            flat(item,depth-1,newArr)
        }else{
            newArr.push(item)
        }
    }
    return newArr
}


module.exports = flat