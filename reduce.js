const items = [1, 2, 3, 4, 5, 5];

function reduce(elements, cb, startingValue) {
  if (!elements) return;

  if (startingValue == undefined) {
    let result = elements[0];

    for (i = 1; i < elements.length; i++) {
      result = cb(result, elements[i]);
    }
    return result;
  } else {
    let result = startingValue;
    for (i = 0; i < elements.length; i++) {
      result = cb(result, elements[i]);
    }

    return result;
  }
}

module.exports = reduce;
